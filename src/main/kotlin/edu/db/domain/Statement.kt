package edu.db.domain

interface Statement {
    fun execute(dbConfiguration: DbConfiguration)
}