package edu.db.domain

class SelectAll(val table: String) : Statement {
    override fun execute(dbConfiguration: DbConfiguration) {
        println("Select all statement")
    }

    override fun toString(): String {
        return "SelectAll(table='$table')"
    }
}