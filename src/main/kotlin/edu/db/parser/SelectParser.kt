package edu.db.parser

import edu.db.domain.SelectAll
import edu.db.domain.Statement
import java.lang.IllegalArgumentException

class SelectParser : Parser {
    private val regex = Regex("(?i)\\s*select\\s*\\*\\s*from\\s*(\\w+)")

    override fun matches(s: String): Boolean {
        return this.regex.matches(s)
    }

    override fun parse(s: String): Statement {
        val matchResult = this.regex.matchEntire(s) ?: throw IllegalArgumentException("Statement doesn't match parser")
        val values = matchResult.groupValues
        val tableName = values[1]
        return SelectAll(tableName)
    }
}