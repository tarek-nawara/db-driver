package edu.db.parser

import edu.db.domain.Insert
import edu.db.domain.Statement

class InsertParser : Parser {
    private val regex = Regex("(?i)insert\\s*into\\s+(\\w+)\\s+values\\s*\\(\\s*((('\\w+')|\\d+)(,\\s*(('\\w+')|\\d+))*)\\s*\\)")

    override fun matches(s: String): Boolean {
        return regex.matches(s)
    }

    override fun parse(s: String): Statement {
        val matchResult = this.regex.matchEntire(s) ?: throw IllegalArgumentException("Statement doesn't match parser")
        val valueList =
                matchResult.groupValues[2]
                        .split(",")
                        .asSequence()
                        .map { it.trim() }
                        .map { value ->
                            if (value.matches("'\\w+'".toRegex()))
                                value.substring(1, value.length - 1)
                            else value
                        }.toList()
        return Insert(valueList)
    }
}