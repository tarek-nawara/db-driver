package edu.db.parser

import edu.db.domain.Statement

interface Parser {
    fun matches(s: String): Boolean
    fun parse(s: String): Statement
}