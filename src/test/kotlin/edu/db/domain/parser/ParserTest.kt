package edu.db.domain.parser

import edu.db.domain.Insert
import edu.db.domain.SelectAll
import edu.db.parser.InsertParser
import edu.db.parser.SelectParser
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class ParserTest {
    @Test
    fun test_parse_select_statement() {
        val statement = "select * from users"
        val parser = SelectParser()
        assertTrue(parser.matches(statement))
        val parseResult = parser.parse(statement)
        assertTrue(parseResult is SelectAll)
        if (parseResult is SelectAll) {
            assertEquals("users", parseResult.table)
        }
    }

    @Test
    fun test_parse_insert_statement() {
        val statement = "insert into users values('test', 123, 'values')"
        val parser = InsertParser()
        assertTrue(parser.matches(statement))
        val parseResult = parser.parse(statement)
        assertTrue(parseResult is Insert)
        if (parseResult is Insert) {
            assertEquals(listOf("test", "123", "values"), parseResult.values)
        }
    }
}